## Matches empty newlines above coolphrase ($6 to ref coolphrase capture group)
(((\r\n|\r|\n)$)|(^(\r\n|\n|\r))|^s*$)(coolphrase)

## Matches MACRO_EXPRESSION(.*, "%s" << e.why());
(MACRO_EXPRESSION\()(.*,{1}\s{1})("%s" << )(e.why\(\)\);)
